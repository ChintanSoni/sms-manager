package com.android.smsmanager.base

import android.support.annotation.LayoutRes
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.smsmanager.R
import com.android.smsmanager.view.viewholder.EmptyViewHolder
import com.android.smsmanager.view.viewholder.SmsViewHolder
import com.android.smsmanager.view.viewholder.ThreadViewHolder

abstract class BaseRecyclerAdapter<DATA, DATAVIEWHOLDER : BaseViewHolder<DATA>, EMPTYVIEWHOLDER : BaseViewHolder<DATA>>(
        private var fragment: Fragment,
        @LayoutRes private var dataViewLayoutResource: Int,
        private var dataViewHolderClass: Class<DATAVIEWHOLDER>,
        @LayoutRes private var emptyViewLayoutResource: Int,
        private var emptyViewHolderClass: Class<EMPTYVIEWHOLDER>) : RecyclerView.Adapter<BaseViewHolder<DATA>>() {

    private val VIEW_TYPE_EMPTY = 0
    private val VIEW_TYPE_DATA = 1

    internal var mDataList: ArrayList<DATA> = ArrayList()

    @StringRes
    private var errorMessage: Int = R.string.error_no_sms

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<DATA> {
        val view = LayoutInflater.from(parent.context).inflate(getLayoutResourceByViewType(viewType), parent, false)
        return getViewHolderByViewType(view, viewType)
    }

    override fun getItemCount(): Int {
        return if (mDataList.isEmpty()) {
            1
        } else {
            mDataList.size
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<DATA>, position: Int) {
        if (holder is SmsViewHolder || holder is ThreadViewHolder) {
            holder.bindData(mDataList[position])
        } else if (holder is EmptyViewHolder) {
            holder.bindData(errorMessage)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (mDataList.isEmpty()) {
            VIEW_TYPE_EMPTY
        } else {
            VIEW_TYPE_DATA
        }
    }

    private fun getLayoutResourceByViewType(viewType: Int): Int {
        return if (viewType == VIEW_TYPE_DATA) {
            dataViewLayoutResource
        } else {
            emptyViewLayoutResource
        }
    }

    private fun getViewHolderByViewType(view: View, viewType: Int): BaseViewHolder<DATA> {
        return if (viewType == VIEW_TYPE_DATA) {
            dataViewHolderClass.getConstructor(Fragment::class.java, View::class.java).newInstance(fragment, view)
        } else {
            emptyViewHolderClass.getConstructor(Fragment::class.java, View::class.java).newInstance(fragment, view)
        }
    }

    fun setData(dataList: ArrayList<DATA>) {
        setData(dataList, R.string.error_no_sms)
    }

    private fun setData(dataList: ArrayList<DATA>, @StringRes errorMessage: Int) {
        this.errorMessage = errorMessage
        setCustomData(mDataList, dataList)
//        mDataList.clear()
//        mDataList.addAll(dataList)
//        notifyDataSetChanged()
    }

    abstract fun setCustomData(oldDataList: ArrayList<DATA>, newDataList: ArrayList<DATA>)

    fun setError(errorMessage: Int) {
        this.errorMessage = errorMessage
        notifyDataSetChanged()
    }
}