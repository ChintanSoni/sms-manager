package com.android.smsmanager.base

import android.support.v7.widget.RecyclerView
import android.view.View

open class BaseViewHolder<DATA>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    protected var data: DATA? = null

    open fun bindData(data: DATA) {
        this.data = data
    }
}