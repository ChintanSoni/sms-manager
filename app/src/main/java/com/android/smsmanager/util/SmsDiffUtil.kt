package com.android.smsmanager.util

import android.support.v7.util.DiffUtil
import com.android.smsmanager.model.database.entity.Sms

class SmsDiffUtil(private val newList: ArrayList<Sms>? = null, private val oldList: ArrayList<Sms>? = null) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList?.get(oldItemPosition)?.sms_id == newList?.get(newItemPosition)?.sms_id
    }

    override fun getOldListSize(): Int {
        return oldList?.size ?: 0
    }

    override fun getNewListSize(): Int {
        return newList?.size ?: 0
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList?.get(oldItemPosition) === newList?.get(newItemPosition)
    }
}