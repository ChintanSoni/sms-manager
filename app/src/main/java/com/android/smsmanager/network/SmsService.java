package com.android.smsmanager.network;

import com.android.smsmanager.model.service.SmsRequest;
import com.android.smsmanager.model.service.SmsResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SmsService {

    @Headers({"Accept: application/json"})
    @POST("/testSubmit")
    Call<SmsResponse> updateSms(@Body SmsRequest smsRequest);

    @Headers({"Accept:application/json"})
    @POST("/testSubmit/{path}")
    Call<SmsResponse> updateSingleSms(@Path(value = "path") String path, @Body SmsRequest smsRequest);

//    @Headers({"Accept:application/json"})
//    @POST("/testSubmit")
//    Call<SmsResponse> updateSingleSms(@Body SmsRequest smsRequest);
}