package com.android.smsmanager.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.android.smsmanager.model.SmsRepository
import com.android.smsmanager.model.background.SmsWorkerUtil
import com.android.smsmanager.model.preference.SimPreference
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class SimChangedReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        // Update SIM number
        updateSimNumberInPreference(context)

        // Sync SMS
        SmsWorkerUtil.queueWork()
    }

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    private fun updateSimNumberInPreference(context: Context) {
        val disposable = SmsRepository.getInstance(context).getAllSimObservable()
                .observeOn(Schedulers.io())
                .subscribe({
                    for (sim in it) {
                        if (sim.sim_id == 0) {
                            SimPreference.storeSim1Number(context, sim.number)
                        }
                        if (sim.sim_id == 1) {
                            SimPreference.storeSim2Number(context, sim.number)
                        }
                    }
                }, {
                    it.printStackTrace()
                })
        compositeDisposable.add(disposable)
    }
}