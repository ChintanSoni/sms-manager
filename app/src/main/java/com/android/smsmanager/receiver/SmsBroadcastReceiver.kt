package com.android.smsmanager.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.telephony.SmsManager
import android.telephony.SmsMessage
import android.util.Log
import com.android.smsmanager.model.SmsRepository
import com.android.smsmanager.model.database.DatabaseConstants
import com.android.smsmanager.model.database.SmsUtil
import com.android.smsmanager.model.database.entity.Sms
import com.android.smsmanager.model.preference.CambodiaNumberPreference
import com.android.smsmanager.model.preference.SimPreference
import com.android.smsmanager.model.service.Message
import com.android.smsmanager.model.service.SmsRequest
import com.android.smsmanager.model.service.SmsResponse
import com.android.smsmanager.network.APIClient
import com.android.smsmanager.network.SmsService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class SmsBroadcastReceiver : BroadcastReceiver() {

    //https://stackoverflow.com/questions/33517461/smsmessage-createfrompdu-is-deprecated-in-android-api-level-23#
    override fun onReceive(context: Context, intent: Intent) {

        sendSmsReceivedBroadcast(context)

        val intentExtras = intent.extras

        if (intentExtras != null) {
            val sms = intentExtras.get(SMS_BUNDLE) as Array<*>
            for (i in sms.indices) {

                val currentSMS: SmsMessage
                currentSMS = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    val format = intentExtras.getString("format")
                    SmsMessage.createFromPdu(sms[i] as ByteArray, format)
                } else {
                    SmsMessage.createFromPdu(sms[i] as ByteArray)
                }

                val slot = fetchSlot(intentExtras)
                //                int sub = -1;

                //                if (intentExtras.containsKey("subscription")) {
                //                    sub = intentExtras.getInt("subscription", -1);
                //                }

                val smsBody = currentSMS.messageBody
                val address = currentSMS.originatingAddress

                if (isNetworkAvailable(context)) {
                    sendSmsToApi(context, currentSMS, slot)
                    //Send sms on service
                    SmsRepository.getInstance(context).getAllSms()
                } else {
                    sendSms(context, smsBody, address)
                }

                //                smsMessageStr += "SMS From: " + address + "\n";
                //                smsMessageStr += smsBody + "\n";
                //                smsMessageStr += " Slot: " + slot + "\n";
                //                smsMessageStr += " Subscription: " + sub + "\n";
            }
        }
    }

    val ACTION_SMS_RECEIVED: String? = "ActionSmsReceived"

    private fun sendSmsReceivedBroadcast(context: Context) {
        val intent = Intent(ACTION_SMS_RECEIVED)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }

    private fun sendSmsToApi(context: Context, currentSMS: SmsMessage, slot: Int) {
        val smsList = ArrayList<Message>()
        val localSmsList = ArrayList<Message>()
        localSmsList.add(SmsUtil.toMessage(Sms(
                0, 0, false,
                System.currentTimeMillis(),
                currentSMS.messageBody,
                currentSMS.originatingAddress,
                DatabaseConstants.FIELD_INBOX,
                false)))
        var smsRequest = SmsRequest(/*SimPreference.getSim1Number(context),
                SimPreference.getSim2Number(context),*/ smsList)

        val apiInterface = APIClient.getClient().create<SmsService>(SmsService::class.java)

        val request: Call<SmsResponse>

        request = if (slot == 0) {
            var smsList = ArrayList<Message>()
            var message = Message(0, currentSMS.messageBody,
                    currentSMS.originatingAddress,
                    formatDate(Calendar.getInstance().time))
            smsList.add(message)
            smsRequest.messages = smsList

            apiInterface.updateSingleSms(SimPreference.getSim1Number(context), smsRequest)
//            apiInterface.updateSingleSms(SimPreference.getSim1Url(context), smsRequest)
//            apiInterface.updateSingleSms(smsRequest)
        } else {
            var smsList = ArrayList<Message>()
            var message = Message(0, currentSMS.messageBody,
                    currentSMS.originatingAddress,
                    formatDate(Calendar.getInstance().time))
            smsList.add(message)
            smsRequest.messages = smsList

            apiInterface.updateSingleSms(SimPreference.getSim2Number(context), smsRequest)
//            apiInterface.updateSingleSms(SimPreference.getSim2Url(context), smsRequest)
//            apiInterface.updateSingleSms(smsRequest)
        }
        Log.d("Forward", request.request().url().toString())
        request.enqueue(object : Callback<SmsResponse> {
            override fun onResponse(call: Call<SmsResponse>?, response: Response<SmsResponse>?) {
            }

            override fun onFailure(call: Call<SmsResponse>?, t: Throwable?) {
            }
        })
    }

    private fun fetchSlot(bundle: Bundle?): Int {
        var slot = -1
        if (bundle != null) {
            val keySet = bundle.keySet()
            for (key in keySet) {
                when (key) {
                    "slot" -> slot = bundle.getInt("slot", -1)
                    "simId" -> slot = bundle.getInt("simId", -1)
                    "simSlot" -> slot = bundle.getInt("simSlot", -1)
                    "slot_id" -> slot = bundle.getInt("slot_id", -1)
                    "simnum" -> slot = bundle.getInt("simnum", -1)
                    "slotId" -> slot = bundle.getInt("slotId", -1)
                    "slotIdx" -> slot = bundle.getInt("slotIdx", -1)
                    else -> if (key.toLowerCase().contains("slot") or key.toLowerCase().contains("sim")) {
                        val value = bundle.getString(key, "-1")
                        if ((value == "0") or (value == "1") or (value == "2")) {
                            slot = bundle.getInt(key, -1)
                        }
                    }
                }
            }
        }

        return slot
    }

    private fun sendSms(context: Context, message: String, sourceAddress: String?) {
        val smsManager = SmsManager.getDefault()
        val destinationAddress = CambodiaNumberPreference.retrieveCambodiaNumber(context)
        if (destinationAddress!!.isNotEmpty() || destinationAddress.isNotBlank()) {
            if (destinationAddress.contains(",")) {
                var phoneNumbers = destinationAddress.split(",")

                for (phoneNumber in phoneNumbers) {
                    if ((phoneNumber!!.isNotEmpty() || phoneNumber.isNotBlank()) && phoneNumber.length > 1) {
                        smsManager.sendTextMessage(phoneNumber, sourceAddress, message, null, null)
                    }
                }
            } else {
                smsManager.sendTextMessage(destinationAddress, sourceAddress, message, null, null)
            }
        }
    }

    private fun isNetworkAvailable(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnected
    }

    companion object {
        const val SMS_BUNDLE = "pdus"
    }

    fun formatDate(time: Date): String {

        var format: String? = "dd-MMM-yyyy hh:mm:ss"

        val sdf = SimpleDateFormat(format)

        return sdf.format(time)
    }
}