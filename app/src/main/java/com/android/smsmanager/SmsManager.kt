package com.android.smsmanager

import android.app.Application
import com.android.smsmanager.model.preference.UrlPreference
import com.android.smsmanager.network.APIClient
import com.facebook.stetho.Stetho

class SmsManager : Application() {
    override fun onCreate() {
        super.onCreate()

        if(UrlPreference.getBaseUrl(this).isBlank()) {
            UrlPreference.storeBaseUrl(this, "http://jsonbin.io")
        }

//        Stetho.initializeWithDefaults(this);

        APIClient.setBaseUrl(UrlPreference.getBaseUrl(this));
    }
}