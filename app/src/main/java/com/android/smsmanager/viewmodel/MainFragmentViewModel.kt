package com.android.smsmanager.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import com.android.smsmanager.model.SimInfo
import com.android.smsmanager.model.SmsRepository
import com.android.smsmanager.model.database.entity.Sms

class MainFragmentViewModel : ViewModel() {

    fun getAllSms(context: Context): LiveData<List<Sms>> {
        return SmsRepository.getInstance(context).getAllSms()
    }

    fun getAllSmsByThreadId(context: Context, threadId: Long): MutableLiveData<List<Sms>> {
        return SmsRepository.getInstance(context).getSmsByThreadId(threadId)
    }

    fun getSims(context: Context): MutableLiveData<List<SimInfo>> {
        return SmsRepository.getInstance(context).getSims()
    }
}