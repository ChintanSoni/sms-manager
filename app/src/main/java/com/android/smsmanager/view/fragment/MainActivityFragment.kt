package com.android.smsmanager.view.fragment

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.smsmanager.R
import com.android.smsmanager.base.BaseRecyclerAdapter
import com.android.smsmanager.model.SmsRepository
import com.android.smsmanager.model.database.entity.Sms
import com.android.smsmanager.model.preference.SimPreference
import com.android.smsmanager.util.SmsDiffUtil
import com.android.smsmanager.view.activity.SmsThreadActivity
import com.android.smsmanager.view.viewholder.EmptyViewHolder
import com.android.smsmanager.view.viewholder.SmsViewHolder
import com.android.smsmanager.viewmodel.MainFragmentViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions

/**
 * A placeholder fragment containing a simple view.
 */
class MainActivityFragment : Fragment(), EasyPermissions.PermissionCallbacks {

    companion object {
        const val RC_SMS_PERMISSION: Int = 100
        const val RATIONALE_SMS = "This app require SMS permissions to work accordingly."
    }

    private var baseRecyclerAdapter: BaseRecyclerAdapter<Sms, SmsViewHolder, EmptyViewHolder>? = null

    private lateinit var viewModel: MainFragmentViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(MainFragmentViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews(view)
        requestSmsPermission()
    }

    override fun onResume() {
        super.onResume()
        registerListener()
    }

    override fun onPause() {
        super.onPause()
        unRegisterListener()
    }

    val ACTION_SMS_RECEIVED: String? = "ActionSmsReceived"
    val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            requestSmsPermission()
        }
    }

    private fun registerListener() {
        LocalBroadcastManager.getInstance(requireContext()).registerReceiver(receiver, IntentFilter(ACTION_SMS_RECEIVED))
    }

    private fun unRegisterListener() {
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(receiver)
    }

    private fun getAllSms() {
        viewModel.getAllSms(requireContext()).observe(this, Observer {
            baseRecyclerAdapter?.setData(ArrayList(it))
        })
        updateSimNumberInPreference(requireContext())
    }

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    private fun updateSimNumberInPreference(context: Context) {
        val disposable = SmsRepository.getInstance(context).getAllSimObservable()
                .observeOn(Schedulers.io())
                .subscribe({
                    for (sim in it) {
                        if (sim.sim_id == 0) {
                            SimPreference.storeSim1Number(context, sim.number)
                        }
                        if (sim.sim_id == 1) {
                            SimPreference.storeSim2Number(context, sim.number)
                        }
                    }
                }, {
                    it.printStackTrace()
                })
        compositeDisposable.add(disposable)
    }

    private fun initViews(view: View) {
        baseRecyclerAdapter = object : BaseRecyclerAdapter<Sms, SmsViewHolder, EmptyViewHolder>(this,
                R.layout.list_item_sms, SmsViewHolder::class.java,
                R.layout.list_item_empty, EmptyViewHolder::class.java) {

            override fun setCustomData(oldDataList: ArrayList<Sms>, newDataList: ArrayList<Sms>) {
                val diffResult = DiffUtil.calculateDiff(SmsDiffUtil(oldDataList, newDataList))
                diffResult.dispatchUpdatesTo(this)
                mDataList.clear()
                mDataList.addAll(newDataList)
            }
        }
        val recyclerView = view.findViewById<RecyclerView>(R.id.rv_main).apply {
            adapter = baseRecyclerAdapter
        }
        recyclerView.addItemDecoration(DividerItemDecoration(requireContext(), (recyclerView.layoutManager as LinearLayoutManager).orientation))
    }

    fun onEmptyViewClick() {
        requestSmsPermission()
    }

    fun onItemClick(data: Sms?) {
        if (data != null) {
            startActivity(SmsThreadActivity.getActivity(requireContext(), data))
        }
    }

    // ------------------------- SMS Permission [START]------------------------
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    private fun requestSmsPermission() {
        if (EasyPermissions.hasPermissions(requireContext(),
                        Manifest.permission.SEND_SMS,
                        Manifest.permission.READ_SMS,
                        Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.READ_PHONE_STATE)) {
            getAllSms()
        } else {
            EasyPermissions.requestPermissions(this, RATIONALE_SMS,
                    RC_SMS_PERMISSION,
                    Manifest.permission.SEND_SMS,
                    Manifest.permission.READ_SMS,
                    Manifest.permission.RECEIVE_SMS,
                    Manifest.permission.READ_PHONE_STATE)
        }
    }

    override fun onPermissionsGranted(requestCode: Int, list: List<String>) {
        getAllSms()
    }

    override fun onPermissionsDenied(requestCode: Int, list: List<String>) {
        baseRecyclerAdapter?.setError(R.string.error_due_to_sms_permission)
        if (EasyPermissions.somePermissionPermanentlyDenied(this, list)) {
            AppSettingsDialog.Builder(this).build().show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            requestSmsPermission()
        }
    }
    // ------------------------- SMS Permission [END]------------------------
}