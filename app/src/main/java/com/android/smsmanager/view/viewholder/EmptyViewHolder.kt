package com.android.smsmanager.view.viewholder

import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.view.View
import android.widget.TextView
import com.android.smsmanager.R
import com.android.smsmanager.base.BaseViewHolder
import com.android.smsmanager.model.database.entity.Sms
import com.android.smsmanager.view.fragment.MainActivityFragment

class EmptyViewHolder(private val fragment: Fragment, itemView: View) : BaseViewHolder<Sms>(itemView), View.OnClickListener {

    override fun onClick(p0: View?) {
        if (fragment is MainActivityFragment) {
            fragment.onEmptyViewClick()
        }
    }

    private var tvEmpty: TextView = itemView.findViewById(R.id.tv_list_item_empty)

    init {
        itemView.setOnClickListener(this)
    }

    fun bindData(@StringRes errorMessage: Int) {
        tvEmpty.setText(errorMessage)
    }
}