package com.android.smsmanager.view.viewholder

import android.graphics.Typeface.BOLD
import android.graphics.Typeface.NORMAL
import android.support.v4.app.Fragment
import android.text.format.DateUtils
import android.view.View
import android.widget.TextView
import com.android.smsmanager.R
import com.android.smsmanager.base.BaseViewHolder
import com.android.smsmanager.model.database.entity.Sms
import com.android.smsmanager.view.fragment.MainActivityFragment

class SmsViewHolder(private var fragment: Fragment, itemView: View) : BaseViewHolder<Sms>(itemView), View.OnClickListener {

    override fun bindData(data: Sms) {
        super.bindData(data)

        tvSender.text = getSender(data)
        tvMessage.text = getMessage(data)
        tvTime.text = getTime(data)

        if (isSmsRead(data)) {
            tvSender.setTypeface(null, NORMAL)
            tvMessage.setTypeface(null, NORMAL)
        } else {
            tvSender.setTypeface(null, BOLD)
            tvMessage.setTypeface(null, BOLD)
        }
    }

    init {
        itemView.setOnClickListener(this)
    }

    private var tvSender: TextView = itemView.findViewById(R.id.tv_list_item_sms_sender)
    private var tvMessage: TextView = itemView.findViewById(R.id.tv_list_item_sms_message)
    private var tvTime: TextView = itemView.findViewById(R.id.tv_list_item_sms_time)

    override fun onClick(p0: View?) {
        (fragment as MainActivityFragment).onItemClick(data)
    }

    private fun isSmsRead(data: Sms): Boolean {
        return data.isRead
    }

    private fun getTime(data: Sms): CharSequence? {
        val date = data.date
        return DateUtils.getRelativeTimeSpanString(date)
    }

    private fun getMessage(data: Sms): CharSequence? {
        return data.body
    }

    private fun getSender(data: Sms): String? {
        val address = data.address
        return if (address.isNotEmpty() && address.isNotBlank()) {
            address
        } else {
            "Unknown"
        }
    }
}