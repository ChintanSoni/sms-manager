package com.android.smsmanager.view.activity

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.android.smsmanager.R
import com.android.smsmanager.model.preference.CambodiaNumberPreference
import com.android.smsmanager.model.preference.SimPreference
import com.android.smsmanager.model.preference.UrlPreference
import com.android.smsmanager.network.APIClient
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.content_settings.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        initViews()
        ll_settings.setOnClickListener {
            showCambodiaNumberFragment()
        }
    }

    private fun showCambodiaNumberFragment() {
        val dialogLayout = layoutInflater.inflate(R.layout.dialog_ask_cambodia_number, null)
        val alertDialog = AlertDialog.Builder(this)
                .setCancelable(false)
                .setView(dialogLayout)
                .create()

        dialogLayout.findViewById<EditText>(R.id.et_ask_cambodia_number).setText(CambodiaNumberPreference
                .retrieveCambodiaNumber(this).toString().trim())

        dialogLayout.findViewById<Button>(R.id.btn_ask_cambodia_number_cancel).setOnClickListener {
            alertDialog.dismiss()
        }
        dialogLayout.findViewById<Button>(R.id.btn_ask_cambodia_number_save).setOnClickListener {
            CambodiaNumberPreference.storeCambodiaNumber(this@SettingsActivity,
                    dialogLayout.findViewById<EditText>(R.id.et_ask_cambodia_number).text.toString())
            initViews()
            alertDialog.dismiss()
        }

        alertDialog.show()
    }

    private fun initViews() {
        if (CambodiaNumberPreference.retrieveCambodiaNumber(this).isNullOrEmpty()) {
            tv_settings_title.text = getString(R.string.set_forwarding_number)
            tv_settings_number.visibility = View.GONE
        } else {
            tv_settings_title.text = getString(R.string.change_forwarding_number)
            tv_settings_number.visibility = View.VISIBLE
            tv_settings_number.text = CambodiaNumberPreference.retrieveCambodiaNumber(this)
        }

        ll_base_url.setOnClickListener {
            showBaseUrlDialogFragment()
        }

        ll_sim1_url.setOnClickListener {
            showSim1UrlDialogFragment()
        }

        ll_sim2_url.setOnClickListener {
            showSim2UrlDialogFragment()
        }

        setBaseUrl()
        setSim1Url()
        setSim2Url()
    }

    private fun setSim2Url() {
        if (SimPreference.getSim2Url(this).isEmpty()) {
            tv_settings_sim2_url_value.text = "Click to set one"
        } else {
            tv_settings_sim2_url_value.text = SimPreference.getSim2Url(this)
        }
    }

    private fun setSim1Url() {
        if (SimPreference.getSim1Url(this).isEmpty()) {
            tv_settings_sim1_url_value.text = "Click to set one"
        } else {
            tv_settings_sim1_url_value.text = SimPreference.getSim1Url(this)
        }
    }

    private fun setBaseUrl() {
        if (UrlPreference.getBaseUrl(this).isEmpty()) {
            tv_settings_base_url_value.text = "Click to set one"
        } else {
            tv_settings_base_url_value.text = UrlPreference.getBaseUrl(this)
        }
    }

    private fun showBaseUrlDialogFragment() {
        val editText = EditText(this)
        editText.setText(UrlPreference.getBaseUrl(this))
        AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Base Url")
                .setView(editText)
                .setPositiveButton(R.string.save) { _, _ ->
                    UrlPreference.storeBaseUrl(this@SettingsActivity, editText.text.toString().trim())
                    APIClient.setBaseUrl(editText.text.toString().trim())
                    setBaseUrl()
                }
                .setNegativeButton(R.string.cancel, null)
                .create().show()
    }

    private fun showSim1UrlDialogFragment() {
        val editText = EditText(this)
        editText.setText(SimPreference.getSim1Url(this))
        AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Sim 2 Url")
                .setView(editText)
                .setPositiveButton(R.string.save) { _, _ ->
                    SimPreference.storeSim1Url(this@SettingsActivity, editText.text.toString().trim())
                    setSim1Url()
                }
                .setNegativeButton(R.string.cancel, null)
                .create().show()
    }

    private fun showSim2UrlDialogFragment() {
        val editText = EditText(this)
        editText.setText(SimPreference.getSim2Url(this))
        AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Sim 2 Url")
                .setView(editText)
                .setPositiveButton(R.string.save) { _, _ ->
                    SimPreference.storeSim2Url(this@SettingsActivity, editText.text.toString().trim())
                    setSim2Url()
                }
                .setNegativeButton(R.string.cancel, null)
                .create().show()
    }
}
