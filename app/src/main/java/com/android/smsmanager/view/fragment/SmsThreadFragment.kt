package com.android.smsmanager.view.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.database.ContentObserver
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.util.DiffUtil
import android.telephony.SmsManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import com.android.smsmanager.R
import com.android.smsmanager.base.BaseRecyclerAdapter
import com.android.smsmanager.model.SimInfo
import com.android.smsmanager.model.database.entity.Sms
import com.android.smsmanager.util.SmsDiffUtil
import com.android.smsmanager.view.activity.SmsThreadActivity
import com.android.smsmanager.view.viewholder.EmptyViewHolder
import com.android.smsmanager.view.viewholder.ThreadViewHolder
import com.android.smsmanager.viewmodel.MainFragmentViewModel
import kotlinx.android.synthetic.main.fragment_sms_thread.*


class SmsThreadFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sms_thread, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private lateinit var viewModel: MainFragmentViewModel

    private var simInfoList: List<SimInfo>? = ArrayList()
    private lateinit var sms: Sms
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sms = activity!!.intent.extras.getParcelable(SmsThreadActivity.ARG_SMS)

        updateView()
        viewModel = ViewModelProviders.of(this).get(MainFragmentViewModel::class.java)
        getAllSmsByThread()
        viewModel.getSims(requireContext()).observe(this, Observer {
            this.simInfoList = it
        })
    }

    private fun getAllSmsByThread() {
        viewModel.getAllSmsByThreadId(requireContext(), sms.threadId).observe(this, Observer {
            baseRecyclerAdapter.setData(ArrayList(it?.asReversed()))
        })
    }

    private fun updateView() {
        if (isValidPhoneNumber(sms.address)) {
            ll_sms_thread.visibility = View.VISIBLE
            tv_sms_thread.visibility = View.INVISIBLE
        } else {
            ll_sms_thread.visibility = View.INVISIBLE
            tv_sms_thread.visibility = View.VISIBLE
        }
    }

    private lateinit var baseRecyclerAdapter: BaseRecyclerAdapter<Sms, ThreadViewHolder, EmptyViewHolder>

    private fun initViews() {
        fab.setOnClickListener {
            selectSimIfDualSim()
        }
        baseRecyclerAdapter = object : BaseRecyclerAdapter<Sms, ThreadViewHolder, EmptyViewHolder>(this,
                R.layout.list_item_message, ThreadViewHolder::class.java,
                R.layout.list_item_empty, EmptyViewHolder::class.java) {

            override fun setCustomData(oldDataList: ArrayList<Sms>, newDataList: ArrayList<Sms>) {
                val diffResult = DiffUtil.calculateDiff(SmsDiffUtil(oldDataList, newDataList))
                diffResult.dispatchUpdatesTo(this)
                mDataList.clear()
                mDataList.addAll(newDataList)
            }
        }
        rv_sms_thread.adapter = baseRecyclerAdapter
    }

    private fun selectSimIfDualSim() {
        if (simInfoList != null && simInfoList!!.isNotEmpty()) {
            val simArrayList = ArrayList<SimInfo>()
            for (simInfo in simInfoList!!) {
                if (simInfo.sim_id >= 0) {
                    simArrayList.add(simInfo)
                }
            }

            if (simArrayList.size >= 2) {
                showSelectSimDialog(simArrayList)
            } else {
                goDefaultSim()
            }
        } else {
            Toast.makeText(requireContext(), "No Sim available.", Toast.LENGTH_LONG).show()
        }
    }

    private fun goDefaultSim() {
        val simArrayList = ArrayList<SimInfo>()
        for (simInfo in simInfoList!!) {
            if (simInfo.sim_id >= 0) {
                simArrayList.add(simInfo)
            }
        }

        sendSms(simArrayList[0].sim_id)
    }

    private fun showSelectSimDialog(simArrayList: ArrayList<SimInfo>) {
        val arrayAdapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_list_item_1)
        for (simInfo in simArrayList) {
            arrayAdapter.add("Sim " + simInfo.sim_id + " : " + simInfo.display_name)
        }

        AlertDialog.Builder(requireContext())
                .setTitle("Choose Sim")
                .setAdapter(arrayAdapter) { p0, p1 ->
                    p0?.dismiss()
                    sendSms(simArrayList[p1].sim_id)
                }.create().show()
    }

    private fun sendSms(sim_id: Int) {
        if (et_sms_thread.text.toString().isNotEmpty()) {
            val smsManager = SmsManager.getDefault()

            if (isValidPhoneNumber(sms.address)) {
                smsManager.sendTextMessage(sms.address, null, et_sms_thread.text.toString(), null, null)
                et_sms_thread.setText("")
            }
        }
    }

    private fun isValidPhoneNumber(address: String): Boolean {
        return android.util.Patterns.PHONE.matcher(address).matches()
    }
}