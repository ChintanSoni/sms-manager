package com.android.smsmanager.view.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import com.android.smsmanager.R
import com.android.smsmanager.model.preference.CambodiaNumberPreference
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
        askForCambodiaNumber()
    }

    private fun initViews() {
        setSupportActionBar(toolbar)
    }

    private fun askForCambodiaNumber() {
        if (shouldAskForCambodiaNumber()) {
            AlertDialog.Builder(this)
                    .setTitle(R.string.app_name)
                    .setMessage(R.string.would_you_like)
                    .setPositiveButton(R.string.yes) { _, _ ->
                        showCambodiaNumberFragment()
                        CambodiaNumberPreference.neverAskFor(this@MainActivity)
                    }
                    .setNegativeButton(R.string.skip) { _, _ ->
                        CambodiaNumberPreference.neverAskFor(this@MainActivity)
                    }.create().show()
        }
    }

    private fun shouldAskForCambodiaNumber(): Boolean {
        return CambodiaNumberPreference.shouldAskFor(this)
    }

    private fun showCambodiaNumberFragment() {
        val dialogLayout = layoutInflater.inflate(R.layout.dialog_ask_cambodia_number, null)
        val alertDialog = AlertDialog.Builder(this)
                .setCancelable(false)
                .setView(dialogLayout)
                .create()

        dialogLayout.findViewById<EditText>(R.id.et_ask_cambodia_number).setText(CambodiaNumberPreference
                .retrieveCambodiaNumber(this).toString().trim())

        dialogLayout.findViewById<Button>(R.id.btn_ask_cambodia_number_cancel).setOnClickListener {
            alertDialog.dismiss()
        }
        dialogLayout.findViewById<Button>(R.id.btn_ask_cambodia_number_save).setOnClickListener {
            CambodiaNumberPreference.storeCambodiaNumber(this@MainActivity,
                    dialogLayout.findViewById<EditText>(R.id.et_ask_cambodia_number).text.toString())
            alertDialog.dismiss()
        }

        alertDialog.show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> {
                startActivity(Intent(this, SettingsActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
