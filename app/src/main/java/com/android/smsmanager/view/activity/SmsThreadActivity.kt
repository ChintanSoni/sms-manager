package com.android.smsmanager.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.telephony.PhoneNumberUtils
import com.android.smsmanager.R
import com.android.smsmanager.model.database.entity.Sms
import kotlinx.android.synthetic.main.activity_sms_thread.*

class SmsThreadActivity : AppCompatActivity() {

    companion object {

        val ARG_SMS: String? = "Sms"

        fun getActivity(context: Context, sms: Sms): Intent {
            val intent = Intent(context, SmsThreadActivity::class.java)
            val bundle = Bundle()
            bundle.putParcelable(ARG_SMS, sms)
            intent.putExtras(bundle)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sms_thread)
        initViews()
        parseIntent()
    }

    private fun parseIntent() {
        val sms = intent.extras.getParcelable<Sms>(ARG_SMS)
        supportActionBar?.title = sms.address
    }

    private fun initViews() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}
