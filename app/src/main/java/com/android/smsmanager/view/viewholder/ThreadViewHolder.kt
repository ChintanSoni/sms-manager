package com.android.smsmanager.view.viewholder

import android.support.v4.app.Fragment
import android.text.format.DateUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.android.smsmanager.R
import com.android.smsmanager.base.BaseViewHolder
import com.android.smsmanager.model.database.DatabaseConstants
import com.android.smsmanager.model.database.entity.Sms

class ThreadViewHolder(private var fragment: Fragment, itemView: View) : BaseViewHolder<Sms>(itemView) {

    override fun bindData(data: Sms) {
        super.bindData(data)

//        println(data.toString())

        if (data.type == DatabaseConstants.FIELD_INBOX) {
            ivLeft.visibility = View.VISIBLE
            ivRight.visibility = View.INVISIBLE
        } else {
            ivLeft.visibility = View.INVISIBLE
            ivRight.visibility = View.VISIBLE
        }
        tvMessage.text = data.body
        tvTime.text = getTime(data)
    }

    private fun getTime(data: Sms): CharSequence? {
        val date = data.date
        return DateUtils.getRelativeTimeSpanString(date)
    }

    private var ivLeft: ImageView = itemView.findViewById(R.id.fab_list_item_left)
    private var ivRight: ImageView = itemView.findViewById(R.id.fab_list_item_right)
    private var tvMessage: TextView = itemView.findViewById(R.id.tv_list_item_message)
    private var tvTime: TextView = itemView.findViewById(R.id.tv_list_item_sms_time)
}