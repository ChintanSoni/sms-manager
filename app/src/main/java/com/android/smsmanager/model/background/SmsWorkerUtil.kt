package com.android.smsmanager.model.background

import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager

class SmsWorkerUtil {
    companion object {
        fun queueWork() {
            val myConstraints = Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build()

            val compressionWork = OneTimeWorkRequestBuilder<SmsWorker>()
                    .setConstraints(myConstraints)
                    .build()

            WorkManager.getInstance()!!.enqueue(compressionWork)
        }
    }
}