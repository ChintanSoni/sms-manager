package com.android.smsmanager.model.service;

import com.google.gson.annotations.SerializedName;

public class SmsResponse {

    @SerializedName("success")
    boolean success;

    @SerializedName("data")
    Object data;

    @SerializedName("id")
    String id;

    @SerializedName("version")
    Integer version;

    @SerializedName("private")
    boolean isPrivate;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public Integer getVersion() {
        return version;
    }
}
