package com.android.smsmanager.model.preference

import android.content.Context

class CambodiaNumberPreference {
    companion object {
        fun storeCambodiaNumber(context: Context, number: String) {
            SharedPrefsUtils.setStringPreference(context, "number", number)
        }

        fun retrieveCambodiaNumber(context: Context): String? {
            return SharedPrefsUtils.getStringPreference(context, "number")
        }

        fun shouldAskFor(context: Context): Boolean {
            return SharedPrefsUtils.getBooleanPreference(context, "ShouldAsk", true)
        }

        fun neverAskFor(context: Context) {
            SharedPrefsUtils.setBooleanPreference(context, "ShouldAsk", false)
        }
    }
}