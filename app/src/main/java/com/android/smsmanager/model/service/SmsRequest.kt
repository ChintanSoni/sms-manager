package com.android.smsmanager.model.service

import com.google.gson.annotations.SerializedName

data class SmsRequest(

//        @SerializedName("sim1")
//        var sim1: String,
//
//        @SerializedName("sim2")
//        var sim2: String,

        @SerializedName("messages")
        var messages: List<Message>? = ArrayList()
)

data class Message(

        @SerializedName("id")
        var id: Long,

        @SerializedName("message")
        var text: String,

        @SerializedName("from")
        var source: String,

        @SerializedName("date_sent")
        var date: String
)