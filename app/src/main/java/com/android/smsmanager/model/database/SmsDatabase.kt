package com.android.smsmanager.model.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.android.smsmanager.model.database.dao.SmsDao
import com.android.smsmanager.model.database.entity.Sms

@Database(entities = [(Sms::class)], version = DatabaseConstants.DATABASE_VERSION)
abstract class SmsDatabase : RoomDatabase() {

    abstract fun smsDao(): SmsDao

    companion object {

        private var INSTANCE: SmsDatabase? = null

        internal fun getDatabase(context: Context): SmsDatabase? {
            if (INSTANCE == null) {
                synchronized(SmsDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                SmsDatabase::class.java, DatabaseConstants.DATABASE_NAME)
                                .build()

                    }
                }
            }
            return INSTANCE
        }
    }

}