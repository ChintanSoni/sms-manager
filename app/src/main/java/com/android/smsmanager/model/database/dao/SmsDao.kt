package com.android.smsmanager.model.database.dao

import android.arch.persistence.room.*
import com.android.smsmanager.model.database.DatabaseConstants
import com.android.smsmanager.model.database.entity.Sms
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface SmsDao {
    @Query("SELECT * from " + DatabaseConstants.TABLE_SMS)
    fun getAllSmsSingle(): Single<List<Sms>>

    @Query("SELECT * from " + DatabaseConstants.TABLE_SMS + " where " + DatabaseConstants.FIELD_SMS_IS_SYNC + " = 0 AND " + DatabaseConstants.FIELD_SMS_TYPE + " = " + DatabaseConstants.FIELD_INBOX)
    fun getAllUnSyncedSms(): List<Sms>

    @Query("SELECT * FROM " + DatabaseConstants.TABLE_SMS + " WHERE " + DatabaseConstants.FIELD_SMS_THREAD_ID + " = :threadId")
    fun loadAllByThreadId(threadId: Long): Single<List<Sms>>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(sms: Sms): Long

    @Insert
    fun insertAll(smsList: List<Sms>)

    @Query("DELETE FROM " + DatabaseConstants.TABLE_SMS + " WHERE " + DatabaseConstants.FIELD_SMS_THREAD_ID + " = :threadId")
    fun deleteByThreadId(threadId: Int)

    @Query("DELETE FROM " + DatabaseConstants.TABLE_SMS)
    fun deleteAllSms()

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun setSync(sms: Sms)
}