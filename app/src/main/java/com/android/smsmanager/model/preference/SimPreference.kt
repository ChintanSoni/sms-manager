package com.android.smsmanager.model.preference

import android.content.Context

object SimPreference {

    const val sim1Number = "Sim1Number"
    const val sim2Number = "Sim2Number"
    const val sim1Url = "Sim1Url"
    const val sim2Url = "Sim2Url"

    fun storeSim1Number(context: Context, value: String) {
        SharedPrefsUtils.setStringPreference(context, sim1Number, value)
    }

    fun getSim1Number(context: Context): String {
        return SharedPrefsUtils.getStringPreference(context, sim1Number)
    }

    fun storeSim2Number(context: Context, value: String) {
        SharedPrefsUtils.setStringPreference(context, sim2Number, value)
    }

    fun getSim2Number(context: Context): String {
        return SharedPrefsUtils.getStringPreference(context, sim2Number)
    }

    fun storeSim1Url(context: Context, value: String) {
        SharedPrefsUtils.setStringPreference(context, sim1Url, value)
    }

    fun getSim1Url(context: Context): String {
        return SharedPrefsUtils.getStringPreference(context, sim1Url)
    }

    fun storeSim2Url(context: Context, value: String) {
        SharedPrefsUtils.setStringPreference(context, sim2Url, value)
    }

    fun getSim2Url(context: Context): String {
        return SharedPrefsUtils.getStringPreference(context, sim2Url)
    }
}