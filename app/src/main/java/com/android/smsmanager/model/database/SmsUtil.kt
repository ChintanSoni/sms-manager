package com.android.smsmanager.model.database

import android.database.Cursor
import com.android.smsmanager.model.database.entity.Sms
import com.android.smsmanager.model.service.Message
import java.text.SimpleDateFormat
import java.util.*

object SmsUtil {

    fun fromCursor(cursor: Cursor): Sms {
        return Sms(cursor.getLong(cursor.getColumnIndex("_id")),
                cursor.getLong(cursor.getColumnIndex("thread_id")),
                cursor.getInt(cursor.getColumnIndex("read")) == 1,
                cursor.getLong(cursor.getColumnIndex("date")),
                cursor.getString(cursor.getColumnIndex("body")),
                cursor.getString(cursor.getColumnIndex("address")),
                cursor.getInt(cursor.getColumnIndex("type")),
                false)
    }

    fun toMessage(sms: Sms): Message {
        val dateTime = Calendar.getInstance()
        dateTime.timeInMillis = sms.date
        return Message(sms.sms_id, sms.body, sms.address, formatDate(dateTime.time))
    }

    fun formatDate(time: Date): String {

        var format: String? = "dd-MMM-yyyy hh:mm:ss"

        val sdf = SimpleDateFormat(format)

        return sdf.format(time)
    }
}