package com.android.smsmanager.model.database.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.android.smsmanager.model.database.DatabaseConstants

@Entity
data class Sms(

        @PrimaryKey
        @ColumnInfo(name = DatabaseConstants.FIELD_SMS_SMS_ID)
        val sms_id: Long,

        @ColumnInfo(name = DatabaseConstants.FIELD_SMS_THREAD_ID)
        val threadId: Long,

        @ColumnInfo(name = DatabaseConstants.FIELD_SMS_IS_READ)
        val isRead: Boolean,

        @ColumnInfo(name = DatabaseConstants.FIELD_SMS_DATE)
        val date: Long,

        @ColumnInfo(name = DatabaseConstants.FIELD_SMS_BODY)
        val body: String,

        @ColumnInfo(name = DatabaseConstants.FIELD_SMS_ADDRESS)
        val address: String,

        @ColumnInfo(name = DatabaseConstants.FIELD_SMS_TYPE)
        val type: Int,

        @ColumnInfo(name = DatabaseConstants.FIELD_SMS_IS_SYNC)
        var isSync: Boolean
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readLong(),
            parcel.readByte() != 0.toByte(),
            parcel.readLong(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readByte() != 0.toByte())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(sms_id)
        parcel.writeLong(threadId)
        parcel.writeByte(if (isRead) 1 else 0)
        parcel.writeLong(date)
        parcel.writeString(body)
        parcel.writeString(address)
        parcel.writeInt(type)
        parcel.writeByte(if (isSync) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Sms> {
        override fun createFromParcel(parcel: Parcel): Sms {
            return Sms(parcel)
        }

        override fun newArray(size: Int): Array<Sms?> {
            return arrayOfNulls(size)
        }
    }
}