package com.android.smsmanager.model

import android.database.Cursor

data class SimInfo(
        val id_: Int,
        val display_name: String,
        val number: String,
        val icc_id: String,
        val sim_id: Int) {

    companion object {
        fun fromCursor(cursor: Cursor): SimInfo {
            val id = cursor.getInt(cursor.getColumnIndex("_id"))
            val sim_id = cursor.getInt(cursor.getColumnIndex("sim_id"))
            val display_name = cursor.getString(cursor.getColumnIndex("display_name"))
            val number = cursor.getString(cursor.getColumnIndex("number"))
            val icc_id = cursor.getString(cursor.getColumnIndex("icc_id"))
            return SimInfo(id, display_name, number, icc_id, sim_id)
        }
    }
}