package com.android.smsmanager.model.database

object DatabaseConstants {
    // Database Name
    const val DATABASE_NAME = "SmsDatabase"

    // Database Version
    const val DATABASE_VERSION = 1

    // Table SMS and fields
    const val TABLE_SMS = "Sms"
    const val FIELD_SMS_ID = "Id"
    const val FIELD_SMS_SMS_ID = "SmsId"
    const val FIELD_SMS_THREAD_ID = "ThreadId"
    const val FIELD_SMS_IS_READ = "IsRead"
    const val FIELD_SMS_DATE = "Date"
    const val FIELD_SMS_BODY = "Body"
    const val FIELD_SMS_ADDRESS = "Address"
    const val FIELD_SMS_TYPE = "Type"
    const val FIELD_SMS_IS_SYNC = "IsSync"

    // Field TYPE constant
    const val FIELD_INBOX = 1
    const val FIELD_SENT = 2
}