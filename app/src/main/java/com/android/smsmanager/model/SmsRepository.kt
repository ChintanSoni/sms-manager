package com.android.smsmanager.model

import android.arch.lifecycle.MutableLiveData
import android.content.ContentResolver
import android.content.Context
import android.database.Cursor
import android.database.DatabaseUtils
import android.net.Uri
import com.android.smsmanager.model.background.SmsWorkerUtil
import com.android.smsmanager.model.database.SmsDatabase
import com.android.smsmanager.model.database.SmsUtil
import com.android.smsmanager.model.database.entity.Sms
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.Single
import io.reactivex.SingleOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class SmsRepository private constructor(context: Context) {

    private var smsDatabase: SmsDatabase? = SmsDatabase.getDatabase(context)
    private var contentResolver: ContentResolver = context.contentResolver

    private var smsListLiveData: MutableLiveData<List<Sms>> = MutableLiveData()
    private var threadListLiveData: MutableLiveData<List<Sms>> = MutableLiveData()
    private var simLiveData: MutableLiveData<List<SimInfo>> = MutableLiveData()

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    companion object {

        private lateinit var smsRepository: SmsRepository

        fun getInstance(context: Context): SmsRepository {
            smsRepository = SmsRepository(context)
            return smsRepository
        }
    }

    fun getAllSms(): MutableLiveData<List<Sms>> {
        val disposable = localSmsObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    smsListLiveData.value = it
                    getRemoteSms()
                }, {
                    it.printStackTrace()
                })
        compositeDisposable.add(disposable)
        return smsListLiveData
    }

    private fun getRemoteSms() {
        val disposable = readSMSContentProviderObservable()
                .flatMap {
                    return@flatMap insertToLocalDatabase(it)
                }
                .flatMap {
                    return@flatMap localSmsObservable()
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    SmsWorkerUtil.queueWork()
                    smsListLiveData.value = it
                }, {
                    it.printStackTrace()
                })
        compositeDisposable.add(disposable)
    }

    private fun readSMSContentProviderObservable(): Single<Cursor> {
        return Single.create(SingleOnSubscribe<Cursor> {
            val cursor = contentResolver.query(Uri.parse("content://sms/inbox"), null, null, null, null)
            it.onSuccess(cursor)
        })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
    }

    private fun insertToLocalDatabase(cursor: Cursor): Single<Int>? {
        return Single.create(SingleOnSubscribe<Int> {
            while (cursor.moveToNext()) {
                try {
                    val sms = SmsUtil.fromCursor(cursor)
                    smsDatabase!!.smsDao().insert(sms)
                } catch (exception: Exception) {
                    exception.stackTrace
                }
            }
            it.onSuccess(1)
        })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
    }

    private fun localSmsObservable(): Single<ArrayList<Sms>> {
        return smsDatabase!!.smsDao().getAllSmsSingle()
                .flatMap {
                    val reversedList = ArrayList(it.asReversed())
                    val hashSet: ArrayList<Long> = ArrayList()
                    val arrayList: ArrayList<Sms> = ArrayList()
                    for (sms in reversedList) {
                        if (!hashSet.contains(sms.threadId)) {
                            hashSet.add(sms.threadId)
                            arrayList.add(sms)
                        }
                    }
                    return@flatMap Single.just(arrayList)
                }
                .subscribeOn(Schedulers.io())
    }

    fun getSmsByThreadId(threadId: Long): MutableLiveData<List<Sms>> {
        getLocalSmsByThreadId(threadId)
        return threadListLiveData
    }

    private fun getLocalSmsByThreadId(threadId: Long) {
        val disposable = smsDatabase!!.smsDao().loadAllByThreadId(threadId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    threadListLiveData.value = it
                }, {
                    it.printStackTrace()
                })
        compositeDisposable.add(disposable)
    }

    fun getSims(): MutableLiveData<List<SimInfo>> {
        val disposable = getAllSimObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    simLiveData.value = it
                }, {
                    it.printStackTrace()
                })
        compositeDisposable.add(disposable)
        return simLiveData
    }

    fun getAllSimObservable(): Observable<ArrayList<SimInfo>> {
        return Observable.create(ObservableOnSubscribe<ArrayList<SimInfo>> {
            val cursor = contentResolver.query(Uri.parse("content://telephony/siminfo/"), null, null, null, null)
            val arrayList = ArrayList<SimInfo>()
            while (cursor.moveToNext()) {
                arrayList.add(SimInfo.fromCursor(cursor))
            }
            it.onNext(arrayList)
            it.onComplete()
        }).subscribeOn(Schedulers.io())
    }
}