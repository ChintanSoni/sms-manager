package com.android.smsmanager.model.background

import androidx.work.Worker
import com.android.smsmanager.model.database.DatabaseConstants
import com.android.smsmanager.model.database.SmsDatabase
import com.android.smsmanager.model.database.SmsUtil
import com.android.smsmanager.model.preference.SimPreference
import com.android.smsmanager.model.service.Message
import com.android.smsmanager.model.service.SmsRequest
import com.android.smsmanager.model.service.SmsResponse
import com.android.smsmanager.network.APIClient
import com.android.smsmanager.network.SmsService
import retrofit2.Call

class SmsWorker : Worker() {
    override fun doWork(): Result {
        fetchAndUploadLocalSms()
        return Result.SUCCESS
    }

    private fun fetchAndUploadLocalSms() {
        val localSmsList = SmsDatabase.getDatabase(applicationContext)!!.smsDao().getAllUnSyncedSms()

        if (localSmsList.isNotEmpty()) {
            val smsList = ArrayList<Message>()
            for (sms in localSmsList) {
                if (sms.type != DatabaseConstants.FIELD_SENT) {
                    smsList.add(SmsUtil.toMessage(sms))
                }
            }

//            if (SimPreference.getSim1Number(applicationContext).isNullOrEmpty() || SimPreference.getSim2Number(applicationContext).isNullOrEmpty()) {
//                return
//            }

            val smsRequest = SmsRequest(
                    /*SimPreference.getSim1Number(applicationContext),
                    SimPreference.getSim2Number(applicationContext),*/
                    smsList)

//            println("SMSMAN Request : " + Gson().toJson(smsRequest))

            val apiInterface = APIClient.getClient().create<SmsService>(SmsService::class.java)
            val request: Call<SmsResponse> = apiInterface.updateSms(smsRequest)
            val response = request.execute()
            if (response.body() != null) {
//                println("SMSMAN Response : " + response.body()!!.version)

                for (sms in localSmsList) {
                    sms.isSync = true
                    SmsDatabase.getDatabase(applicationContext)!!.smsDao().setSync(sms)
                }
            }

//            if (response.errorBody() != null) {
//                println("SMSMAN Error Response : " + response.errorBody()!!.string())
//            }
        }
    }
}