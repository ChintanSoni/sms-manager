package com.android.smsmanager.model.preference

import android.content.Context

object UrlPreference {

    const val baseUrl = "baseUrl"

    fun storeBaseUrl(context: Context, value: String) {
        SharedPrefsUtils.setStringPreference(context, baseUrl, value)
    }

    fun getBaseUrl(context: Context): String {
        return SharedPrefsUtils.getStringPreference(context, baseUrl)
    }
}